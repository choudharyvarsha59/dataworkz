from flask import jsonify,abort,request
from app import app
from app.models.stock import Stock
import json

@app.route('/avg_price',methods=['GET'])
def avg_price():
	stock = Stock()
	avgprice = stock.get_avg_price()
	return jsonify({"avg_price": avgprice})


@app.route('/avg_price_with_date',methods=['GET'])
def avg_price_with_date():
	stock = Stock()
	request_data = request.args
	start_date = request_data.get('start_date')
	end_date = request_data.get('end_date')
	if not start_date:
		abort(400, "start_date missing")
	if not end_date:
		abort(400, "end_date missing")
	avgprice = stock.get_avg_price_with_date(start_date, end_date)
	return jsonify({"avg_price": avgprice})

@app.route('/avg_price_volume',methods=['GET'])
def avg_price_volume():
	stock = Stock()
	avgprice_volume = stock.get_avg_price_volume()
	return jsonify({"avg_price_volume": avgprice_volume})

@app.route('/avg_price_volume_with_date',methods=['GET'])
def avg_price_volume_with_date():
	stock = Stock()
	request_data = request.args
	start_date = request_data.get('start_date')
	end_date = request_data.get('end_date')
	if not start_date:
		abort(400, "start_date missing")
	if not end_date:
		abort(400, "end_date missing")
	avgprice_volume = stock.get_avg_price_volume_with_date(start_date, end_date)
	return jsonify({"avg_price_volume": avgprice_volume})

@app.route('/avg_weighted_price',methods=['GET'])
def avg_weighted_price():
	stock = Stock()
	request_data = request.args
	start_date = request_data.get('start_date')
	end_date = request_data.get('end_date')
	if not start_date:
		abort(400, "start_date missing")
	if not end_date:
		abort(400, "end_date missing")
	avgweighted_price = stock.get_avg_weighted_price(start_date, end_date)
	return jsonify({"avg_weighted_price": avgweighted_price})
