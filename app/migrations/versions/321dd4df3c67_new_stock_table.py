"""new stock table

Revision ID: 321dd4df3c67
Revises: 
Create Date: 2017-04-26 22:01:33.159625

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '321dd4df3c67'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'stock',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('date', sa.Date, nullable=False),
        sa.Column('stock', sa.String(50), nullable=False),
        sa.Column('price', sa.Float, nullable=False),
        sa.Column('volume', sa.Float, nullable=False)
    )

    op.create_unique_constraint('idx_stock_date','stock', ['date', 'stock'])

def downgrade():
    op.drop_table('stock')
    op.drop_constraint('idx_stock_date', 'stock')
