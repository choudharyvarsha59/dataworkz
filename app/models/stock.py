from app import db
from sqlalchemy import func


class Stock(db.Model):
    __tablename__ = 'stock'

    id=db.Column(db.Integer ,primary_key=True)
    date=db.Column(db.Date,nullable=False)
    stock=db.Column(db.String(50),nullable=False)
    price=db.Column(db.Float,nullable=False)
    volume=db.Column(db.Float,nullable=False)

    def add_new_stock(self,data):
        try:
            stock=Stock()
            stock.date=data['date']
            stock.stock=data['stock']
            stock.price=data['price']
            stock.volume=data['volume']
            db.session.add(stock)
            db.session.flush()
            db.session.commit()
            return stock.id
        except Exception, e:
            db.session.rollback()

    def get_avg_price(self):
        try:
            avg_price = db.session.query(func.avg(Stock.price)).all()
            return avg_price[0][0]
        except Exception, e:
            db.session.rollback()
            raise e
        finally:
            db.session.close()


    def get_avg_price_with_date(self, start_date, end_date):
        try:
            avg_price = db.session.query(func.avg(Stock.price)).filter(
                Stock.date>=start_date, Stock.date<=end_date).all()
            return avg_price[0][0]
        except Exception, e:
            db.session.rollback()
            raise e
        finally:
            db.session.close()
        

    def get_avg_price_volume(self):
        try:
            avg_price_volume = db.session.query(func.avg(Stock.price*Stock.volume)).all()
            return avg_price_volume[0][0]
        except Exception, e:
            db.session.rollback()
            raise e
        finally:
            db.session.close()


    def get_avg_price_volume_with_date(self, start_date, end_date):
        try:
            avg_price_volume = db.session.query(func.avg(Stock.price*Stock.volume)).filter(
                Stock.date>=start_date, Stock.date<=end_date).all()
            return avg_price_volume[0][0]
        except Exception, e:
            db.session.rollback()
            raise e
        finally:
            db.session.close()

    def get_avg_weighted_price(self, start_date, end_date):
        try:
            avg_weighted_price = db.session.query(
                func.avg(Stock.price * Stock.volume)/func.avg(Stock.volume)
                ).filter(Stock.date>=start_date, Stock.date<=end_date).all()
            return avg_weighted_price[0][0]
        except Exception, e:
            db.session.rollback()
            raise e
        finally:
            db.session.close()
