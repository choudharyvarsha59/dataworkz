import requests
import json
import datetime
from app.models.stock import Stock

def fetch_and_save(stock_id):
    data = fetch_data(stock_id)
    price_list = data['price']
    volume_list = data['volume']

    datewise_data = {}
    for price in price_list:
        datewise_data[price['date']] = {'price': price['value']}

    for volume in volume_list:
        datewise_data[volume['date']]['volume'] = volume['value']

    for date in datewise_data:
        record = {}
        record['price'] = datewise_data[date]['price']
        record['stock'] = stock_id
        record['volume'] = datewise_data[date]['volume']
        record['date'] = datetime.datetime.fromtimestamp(
            int(date)/1000.0).strftime('%Y/%m/%d')
        stock = Stock()
        stock.add_new_stock(record)


def fetch_data(stock_id):
    url = 'https://sgx-premium.wealthmsi.com/sgx/company/priceHistory'
    body = {"id":stock_id}
    kwargs = {'data':json.dumps(body)}
    response = requests.post(url,**kwargs)
    return response.json()

stock_id = raw_input("enter stock id: ")
print "fetch stock data for stock id:",stock_id
fetch_and_save(stock_id)
